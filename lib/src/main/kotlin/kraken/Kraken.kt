package kraken

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import kraken.gson.CurrencyDeserializer
import kraken.ktor.KrakenSign
import kraken.model.*
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.text.DecimalFormat

class KrakenMarketData(private val baseUrl: String = "https://api.kraken.com") {

    private val client = HttpClient {
        install(JsonFeature) {
            serializer = GsonSerializer {
                setPrettyPrinting()
                registerTypeAdapter(Currency::class.java, CurrencyDeserializer())
            }
        }
//        install(Logging)
    }

    suspend fun assets(): Result<Map<Currency, AssetDetails>> {
        return unwrapKrakenResponse(client.submitForm(url = "$baseUrl/0/public/Assets"))
    }

    suspend fun assetPairs(): Result<Map<String, AssetPairDetails>> {
        return unwrapKrakenResponse(client.submitForm(url = "$baseUrl/0/public/AssetPairs"))
    }
}

class Kraken(
    private val key: String,
    private val secret: String,
    private val baseUrl: String = "https://api.kraken.com"
) {

    private val log = LoggerFactory.getLogger("kraken-exchange")


    private val client = HttpClient {
        install(KrakenSign) {
            this.apiKey = key
            this.apiSecret = secret
        }
        install(JsonFeature) {
            serializer = GsonSerializer {
                setPrettyPrinting()
                registerTypeAdapter(Currency::class.java, CurrencyDeserializer())
            }
        }
//        install(Logging)
    }

    private val gson = GsonBuilder().create()

    val userData = UserData()
    val userTrades: UserTrades = UserTrades()


    inner class UserData {
        suspend fun balance(): Result<Map<Currency, BigDecimal>> {
            return unwrapKrakenResponse(client.submitForm(url = "$baseUrl/0/private/Balance"))
        }

        suspend fun closedOrders(): Result<ClosedOrders> {
            return unwrapKrakenResponse(client.submitForm(url = "$baseUrl/0/private/ClosedOrders"))
        }

        suspend fun openOrders(): Result<OpenOrders> {
            return unwrapKrakenResponse(client.submitForm(url = "$baseUrl/0/private/OpenOrders"))
        }

        suspend fun tradeHistory(): Result<Trades> {
            return unwrapKrakenResponse(client.submitForm(url = "$baseUrl/0/private/TradesHistory"))
        }
    }


    inner class UserTrades {

        private val decimalFormat = DecimalFormat("#.##########")

        suspend fun sell(volume: Double) {
            return addOrder("sell", volume, "XXBTZGBP")
        }

        suspend fun buy(volume: Double) {
            return addOrder("buy", volume, "XXBTZGBP")
        }

        private suspend fun addOrder(direction: String, volume: Double, assetPair: String) {
            val responseString: String = client.submitForm(
                url = "https://api.kraken.com/0/private/AddOrder",
                formParameters = Parameters.build {
                    append("type", direction)
                    append("pair", assetPair)
                    append("ordertype", "market")
                    append("volume", decimalFormat.format(volume))
                },
            )
            val response = gson.fromJson(responseString, JsonObject::class.java)

            val errors = response.get("error").asJsonArray
            if (!errors.isEmpty) {
                throw Exception(errors.toString())
            }
        }
    }
}


private fun <T> unwrapKrakenResponse(response: KrakenResponse<T?>): Result<T> {
    return if (response.result != null && response.error.isEmpty()) {
        Result.success(response.result)
    } else {
        Result.failure(KrakenErrors(response.error))
    }
}