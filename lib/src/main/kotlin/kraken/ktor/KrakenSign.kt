package kraken.ktor

import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.util.*
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


class KrakenSign(
    val apiKey: String,
    apiSecret: String
) {

    private val digest = MessageDigest.getInstance("SHA-256")
    private val sha512Hmac = Mac.getInstance("HmacSHA512")

    init {
        sha512Hmac.init(SecretKeySpec(Base64.getDecoder().decode(apiSecret), "HmacSHA512"))
    }

    private fun hash(input: String): ByteArray {
        return digest.digest(input.toByteArray(StandardCharsets.UTF_8))
    }

    private fun hmac(input: ByteArray): ByteArray {
        return sha512Hmac.doFinal(input)
    }

    fun sign(nonce: String, formData: String, encodedPath: String): String {
        val hashedData = hash(nonce + formData)
        return Base64.getEncoder().encodeToString(hmac(encodedPath.toByteArray(StandardCharsets.UTF_8) + hashedData))
    }

    class Config {
        var apiKey: String = ""
        var apiSecret: String = ""
    }

    companion object Feature : HttpClientFeature<Config, KrakenSign> {
        override val key: AttributeKey<KrakenSign> = AttributeKey("huh?")

        override fun prepare(block: Config.() -> Unit): KrakenSign {
            val config = Config().apply(block)
            return KrakenSign(config.apiKey, config.apiSecret)
        }

        override fun install(feature: KrakenSign, scope: HttpClient) {
            scope.requestPipeline.intercept(HttpRequestPipeline.Transform) {
                val nonce = System.currentTimeMillis().toString()

                val formData = context.body as? FormDataContent ?: FormDataContent(parametersOf())

                val formDataWithNonce = FormDataContent(parametersOf("nonce" to listOf(nonce)) + formData.formData)

                val encodedPath = context.url.encodedPath

                context.header("API-Key", feature.apiKey)
                context.header("API-Sign", feature.sign(nonce, formDataWithNonce.formData.formUrlEncode(), encodedPath))

                proceedWith(formDataWithNonce)
            }
        }
    }
}