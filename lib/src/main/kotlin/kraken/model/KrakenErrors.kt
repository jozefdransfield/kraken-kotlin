package kraken.model

data class KrakenErrors(val errors: List<String>) : Exception()