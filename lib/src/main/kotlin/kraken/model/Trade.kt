package kraken.model

data class Trade(
    val ordertxid: String,
    val postxId: String,
    val pair: String,
    val time: Double,
    val type: String,
    val orderType: String,
    val price: Double,
    val cost: Double,
    val fee: Double,
    val vol: Double,
    val margin: Double,
    val misc: String
)