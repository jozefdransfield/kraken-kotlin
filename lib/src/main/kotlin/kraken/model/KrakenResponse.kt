package kraken.model

data class KrakenResponse<T>(
    val error: List<String>,
    val result: T?,
)