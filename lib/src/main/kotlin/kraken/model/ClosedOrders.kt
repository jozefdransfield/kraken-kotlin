package kraken.model

data class ClosedOrders(
    val closed: Map<String, Order>,
    val count: Int
)