package kraken.model

data class Trades(
    val trades: Map<String, Trade>,
    val count: Int
)