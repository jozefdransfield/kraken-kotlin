package kraken.model

data class Order(
    val refid: String,
    val status: String
)