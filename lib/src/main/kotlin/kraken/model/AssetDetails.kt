package kraken.model

data class AssetDetails(
    val aclass: String,
    val altname: String,
    val decimals: Int,
    val display_decimals: Int
)