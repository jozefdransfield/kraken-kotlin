package kraken.model

data class OpenOrders(
    val open: Map<String, Order>,
    val count: Int
)