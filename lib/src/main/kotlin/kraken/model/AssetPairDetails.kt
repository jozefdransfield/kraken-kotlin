package kraken.model

data class AssetPairDetails(
    val altname: String,
    val wsname: String,
    val aclass_base: String,
    val base: Currency,
    val aclass_quote: String,
    val quote: Currency,
    val lot: String,
)