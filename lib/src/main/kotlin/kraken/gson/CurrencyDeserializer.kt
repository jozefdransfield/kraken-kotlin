package kraken.gson

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import kraken.model.Currency
import java.lang.reflect.Type

class CurrencyDeserializer : JsonDeserializer<Currency> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Currency? {
        if (json != null) {
            return Currency.fromName(json.asString)
        }
        return null
    }
}