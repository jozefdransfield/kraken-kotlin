plugins {
    id("org.jetbrains.kotlin.jvm") version "1.5.21"
    `maven-publish`
    `java-library`
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("io.ktor:ktor-client-auth:1.6.1")
    implementation("io.ktor:ktor-client-core:1.6.1")
    implementation("io.ktor:ktor-client-json:1.6.1")
    implementation("io.ktor:ktor-client-gson:1.6.1")
    implementation("com.google.code.gson:gson:2.8.7")
    implementation("io.ktor:ktor-client-cio:1.6.1")
    implementation("ch.qos.logback:logback-classic:1.2.1")
    implementation("io.ktor:ktor-client-logging:1.6.1")
    implementation("org.jetbrains.kotlin:kotlin-reflect")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.elmo"
            artifactId = "kraken-kotlin"
            version = "0.1." + System.getenv("CI_JOB_ID")
            from(components["java"])
        }
    }
    repositories {
        maven {
            url =  uri("https://gitlab.com/api/v4/projects/29253010/packages/maven")
            name = "Gitlab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}